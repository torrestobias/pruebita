package ar.com.unqui.desktop

import ar.com.unqui.desktop.appModel.CommerceAppModel
import ar.com.unqui.desktop.appModel.ItemAppModel
import org.uqbar.arena.kotlin.extensions.*
import org.uqbar.arena.widgets.Button
import org.uqbar.arena.widgets.NumericField
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.windows.SimpleWindow
import org.uqbar.arena.windows.WindowOwner
import org.uqbar.lacar.ui.model.Action


class WindowsStock (owner: WindowOwner, commerceAppModel: CommerceAppModel) : SimpleWindow<CommerceAppModel>(owner,commerceAppModel) {

    override fun addActions(actionsPanel: Panel?) {

    }

    override fun createFormPanel(mainPanel: Panel) {
        title = "Control de Stock"

        table<ItemAppModel>(mainPanel) with  {
            bindItemsTo("items")
            bindSelectionTo("selectItem")
            column {
                title = "Nombre"
                fixedSize = 250
                bindContentsTo("name")

            }
            column {
                title = "Nombre"
                fixedSize = 250
                bindContentsTo("stock")
            }
        }
        NumericField (mainPanel) with {
            height = 20
            width = 45
            bindTo("input")
        }

        Button(mainPanel) with {
            text = "Extraer"
            onClick(Action { ConfirmDialog(owner, modelObject).open() })
        }



    }

}

