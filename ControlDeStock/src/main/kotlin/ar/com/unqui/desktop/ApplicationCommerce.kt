package ar.com.unqui.desktop

import ar.com.unqui.desktop.appModel.CommerceAppModel
import org.uqbar.arena.Application
import org.uqbar.arena.windows.Window

class ApplicationCommerce : Application() {
    override fun createMainWindow(): Window<*> {
        return WindowsStock(this, CommerceAppModel())
    }
}

fun main (){
    ApplicationCommerce().start()
}