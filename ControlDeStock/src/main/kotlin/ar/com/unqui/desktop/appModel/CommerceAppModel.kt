package ar.com.unqui.desktop.appModel

import ar.com.unqui.Commerce
import ar.com.unqui.CommerceFactory
import org.uqbar.commons.model.annotations.Observable

@Observable
class CommerceAppModel{
    val system : Commerce = CommerceFactory.makeBankSystem()
    var items = initItems()
    var selectItem : ItemAppModel? = null
    var input : Int = 0


    //Los valores que vienen en el modelo items, recorro todos los valores del modelo
    //y por cada uno de ellos instancio un appModel y lo paso a una lista y los devuelvo
    fun initItems (): MutableList<ItemAppModel>{
        return system.items.map { ItemAppModel (it) }.toMutableList()
    }

    fun extract (itemAppModel: ItemAppModel?, value : Int){
        selectItem?.stock = selectItem?.egress(value)!!
    }
}