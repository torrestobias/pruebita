package ar.com.unqui.desktop

import ar.com.unqui.desktop.appModel.CommerceAppModel
import org.uqbar.arena.kotlin.extensions.caption
import org.uqbar.arena.kotlin.extensions.text
import org.uqbar.arena.kotlin.extensions.with
import org.uqbar.arena.widgets.Button
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.windows.Dialog
import org.uqbar.arena.windows.WindowOwner
import org.uqbar.lacar.ui.model.Action

class ConfirmDialog (owner: WindowOwner, model : CommerceAppModel) : Dialog<CommerceAppModel>(owner, model) {
    override fun createFormPanel(mainPanel: Panel?) {
        Label(mainPanel) with {
            text = "Está seguro que desea realizar la operacion de extraccion de stock?"
        }
        Button(mainPanel) with {
            caption = "Aceptar"
            onClick(Action {
                extraer()
                close() })
        }
        Button(mainPanel) with {
            caption = "Cancelar"
            onClick(Action {close()  })
        }

    }

    fun extraer(){
        modelObject.extract(modelObject.selectItem,modelObject.input)
    }

}