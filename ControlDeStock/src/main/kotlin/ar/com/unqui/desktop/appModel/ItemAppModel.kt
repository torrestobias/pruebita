package ar.com.unqui.desktop.appModel

import ar.com.unqui.Item
import ar.com.unqui.NoStockException
import org.uqbar.commons.model.annotations.Observable
import org.uqbar.commons.model.exceptions.UserException

@Observable
class ItemAppModel (var item: Item){
    var name : String = ""
    var stock : Int = 0
    var model : Item = item

    init {
            this.name = item.name
            this.stock = item.stock
    }

    fun entry (value: Int) : Int{
        return this.model.entry(value)
    }

    fun egress (value: Int):Int{
        try {
        return this.model.egress(value)
        } catch (e: NoStockException){
            throw UserException(e.message)
        }
    }

}