package ar.com.unqui

object CommerceFactory {
    private val system = Commerce()

    init {
        var item1 = Item ("Arroz XXXX",23)
        var item2 = Item("Fideos YYYY",11)
        var item3 = Item("Lentejas NNNN",50)
        var item4 = Item("Lamparas MMMM", 0)
        var item5 = Item("Pañales RRRR",1)

        system.addItem(item1)
        system.addItem(item2)
        system.addItem(item3)
        system.addItem(item4)
        system.addItem(item5)
    }

    fun makeBankSystem() = system
}