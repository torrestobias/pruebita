package ar.com.unqui

class Item(var name : String, var stock : Int) {
    fun entry (count: Int) :Int {
        stock += count
        return stock
    }

    fun egress(amount: Int): Int {
        if(stock < amount){
            throw NoStockException("No hay stock suficiente para vender")
        }
        stock -= amount
        return stock
    }
}