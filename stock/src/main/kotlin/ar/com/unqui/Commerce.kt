package ar.com.unqui

class Commerce {
    val items : MutableList<Item> = mutableListOf<Item>()

    fun addItem (item: Item){
        items.add(item)
    }
}