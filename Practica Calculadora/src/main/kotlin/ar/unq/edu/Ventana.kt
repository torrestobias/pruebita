package ar.unq.edu

import org.uqbar.arena.kotlin.extensions.*
import org.uqbar.arena.widgets.Button
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.widgets.TextBox
import org.uqbar.arena.windows.SimpleWindow
import org.uqbar.arena.windows.WindowOwner
import org.uqbar.lacar.ui.model.Action

class Ventana : SimpleWindow <Entidad>{

    constructor(owner: WindowOwner, model: Entidad): super(owner,model)

    override fun addActions(actionsPanel: Panel?) {

    }

    override fun createFormPanel(mainPanel: Panel?) {
        title = "Calculadora"

        Label(mainPanel) with {
            text= "Ingrese el Operador 1"
        }

        TextBox(mainPanel) with {
            bindTo("operador1")
            width= 230
        }

        Label(mainPanel) with {
            text= "Ingrese el Operador 2"
        }

        TextBox(mainPanel) with {
            bindTo("operador2")
            width= 230
        }

        Button(mainPanel) with {
            caption = "Sumar"
            onClick(Action { operacionSuma() })
        }

        Button(mainPanel) with {
            caption = "Restar"
            onClick(Action { operacionResta() })
        }

        Button(mainPanel) with {
            caption = "Multiplicar"
            onClick(Action { operacionMultiplcar() })
        }

        Button(mainPanel) with {
            caption = "Dividir"
            onClick(Action { operacionDivision() })
        }

        Label(mainPanel) with {
            text= "Tu resultado es:"
        }

        TextBox(mainPanel)with {
            bindTo("resultado")
            width= 230
        }

    }

    private fun operacionSuma(){
        modelObject.sumar()
    }

    private fun operacionResta(){
        modelObject.restar()
    }

    private fun operacionMultiplcar(){
        modelObject.multiplicar()
    }

    private fun operacionDivision(){
        modelObject.dividir()
    }


}