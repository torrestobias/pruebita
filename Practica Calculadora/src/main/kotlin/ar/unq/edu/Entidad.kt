package ar.unq.edu

import org.uqbar.commons.model.annotations.Observable
import org.uqbar.commons.model.exceptions.UserException

@Observable
class Entidad {

    var operador1 : Double = 0.0
    var operador2: Double = 0.0
    var resultado: Double = 0.0

    fun sumar(){
        resultado = operador1 + operador2
    }

    fun restar() {
        resultado = operador1 - operador2
    }

    fun multiplicar() {
        resultado = operador1 * operador2
    }

    fun dividir() {
        if(this.operador2==0.0){
            var e : NoDivisionZeroException = NoDivisionZeroException("No podes realizar una division por 0")
            throw UserException(e.message)
        }
        resultado = operador1 / operador2
    }
}