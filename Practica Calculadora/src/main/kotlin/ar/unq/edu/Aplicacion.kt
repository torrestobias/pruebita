package ar.unq.edu

import org.uqbar.arena.Application
import org.uqbar.arena.windows.Window

class Aplicacion : Application(){
    override fun createMainWindow(): Window<*> {
        var entidad = Entidad()
        return Ventana(this, entidad)
    }
}

fun main(){
    Aplicacion().start()
}